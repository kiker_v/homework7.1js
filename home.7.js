function filterBy(list, type){
    let newList = []
    list.forEach(element => {
        if(typeof element != type){
            newList.push(element)
        }
    }); 
    return newList
    
}
let r = filterBy([1, 'hello', true, [], [1, 2, 4, 5], 4.45], 'number')
console.log(r)